GITDIR := 'home'

all:
	echo 'hello world'

software:
	sudo apt-get install zsh python-software-properties fontconfig python-pip python-dev build-essential -y
	sudo add-apt-repository ppa:git-core/ppa -y
	sudo apt-get update
	sudo apt-get install git -y
	sudo pip install --upgrade pip
	sudo pip install --upgrade virtualenv
	if test -d .home; then echo 'Home exists.'; else git clone --recursive https://kbrandow@bitbucket.org/kbrandow/home.git .home; fi
	ln -fs .home/.vimrc .vimrc
	ln -fs .home/.zshrc .zshrc
	ln -fs .home/.vim .vim
	if test -d ~/.oh-my-zsh; then echo 'ZSH Installed.'; else git clone git://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh; fi
	mkdir -p .zshthemes
	if test -d .zshthemes/oh-my-zsh-solarized-powerline-theme; then echo 'Oh My ZSH Solarized Powerline Theme installed.'; else git clone https://github.com/KuoE0/oh-my-zsh-solarized-powerline-theme.git .zshthemes/oh-my-zsh-solarized-powerline-theme; fi
	ln -fs ~/.zshthemes/oh-my-zsh-solarized-powerline-theme/solarized-powerline.zsh-theme ~/.oh-my-zsh/themes
	pip install --user git+git://github.com/Lokaltog/powerline
	wget https://github.com/Lokaltog/powerline/raw/develop/font/PowerlineSymbols.otf
	wget https://github.com/Lokaltog/powerline/raw/develop/font/10-powerline-symbols.conf
	mkdir -p .fonts
	mv PowerlineSymbols.otf ~/.fonts
	fc-cache -vf ~/.fonts
