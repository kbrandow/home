set nocompatible
call pathogen#infect('/Users/kristoferbrandow/.vim/bundle/{}')
set laststatus=2
let g:Powerline_theme="skwp"
let g:Powerline_colorscheme="skwp"
let g:Powerline_symbols = 'fancy'
let g:dbgPavimPort = 9000
let g:dbgPavimBreakAtEntry = 0
let g:Vdebugport = 9009
syntax enable
set background=dark
colorscheme solarized
set pastetoggle=<F2>

if has("autocmd")
  " Drupal *.module and *.install files.
  augroup module
    autocmd BufRead,BufNewFile *.module set filetype=php
    autocmd BufRead,BufNewFile *.install set filetype=php
    autocmd BufRead,BufNewFile *.test set filetype=php
    autocmd BufRead,BufNewFile *.inc set filetype=php
    autocmd BufRead,BufNewFile *.profile set filetype=php
    autocmd BufRead,BufNewFile *.view set filetype=php
  augroup END
endif
autocmd BufRead,BufNewFile *.njs set filetype=javascript
au BufRead,BufNewFile *.conf set ft=nginx
au BufRead,BufNewFile /usr/local/etc/nginx/*, if &ft == '' | setfiletype nginx | endif
autocmd BufRead,BufNewFile *.twig set filetype=html.twig
autocmd BufRead,BufNewFile */_content/templates/*.html set filetype=html.twig
syntax on
set hidden
set expandtab
set tabstop=4
set shiftwidth=4
set autoindent
set copyindent
set hlsearch
set smartindent
set number
set backspace=indent,eol,start
set incsearch
set wrap

set history=1000         " remember more commands and search history
set undolevels=1000      " use many muchos levels of undo
set wildignore=*.swp,*.bak,*.pyc,*.class
set title                " change the terminal's title
set visualbell           " don't beep
set noerrorbells         " don't beep

set nobackup
set noswapfile

filetype indent plugin on
"set tw=80
set nofoldenable    " disable folding
let mapleader = ","
let g:vim_markdown_folding_disabled=1
let g:EasyMotion_leader_key = '<Leader>m'
nmap <F8> :TagbarToggle<CR>

" Quickly edit/reload the vimrc file
nmap <silent> <leader>ev :tabe $MYVIMRC<CR>
nmap <silent> <leader>sv :so $MYVIMRC<CR>

" Let's use a semi colon instead of colon
nnoremap ; :

" Easy window navigation
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

" Quick Escape!
inoremap jk <esc>

" Clear those searches!
nmap <silent> ,/ :nohlsearch<CR>

" For when I forgot to sudo
cmap w!! w !sudo tee % >/dev/null

" Danger, Will Robinson
" You've disabled your arrow keys
map <up> <nop>
map <down> <nop>
map <left> <nop>
map <right> <nop>

" Syntastic configuration!
let g:syntastic_php_checkers=['php', 'phpmd']

" Don't start vim instant markdown automatically
let g:instant_markdown_autostart = 0

" Go stuff
let g:go_fmt_command = "goimports"
let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_structs = 1
let g:go_auto_type_info = 1
let g:go_fmt_experimental = 1
au FileType go nmap <Leader>s <Plug>(go-implements)
au FileType go nmap <Leader>gd <Plug>(go-doc)
au FileType go nmap <Leader>gv <Plug>(go-doc-vertical)
au FileType go nmap <leader>r <Plug>(go-run)
au FileType go nmap <leader>b <Plug>(go-build)
au FileType go nmap <leader>t <Plug>(go-test)
au FileType go nmap <leader>c <Plug>(go-coverage)
au FileType go nmap <Leader>ds <Plug>(go-def-split)
au FileType go nmap <Leader>dv <Plug>(go-def-vertical)
au FileType go nmap <Leader>dt <Plug>(go-def-tab)
au FileType go nmap <Leader>e <Plug>(go-rename)

" App Engine is an Asshole.
if has("autocmd")
    autocmd BufRead,BufNewFile *.appengine.go let g:syntastic_go_go_exec="goapp"
endif

let g:UltiSnipsExpandTrigger="<c-e>"
let g:UltiSnipsJumpForwardTrigger="<c-e>"

" Welcome to Vim Airlines!
let g:airline_solarized_bg="dark"
let g:airline_theme="solarized"
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#show_buffers = 1
let g:airline#extensions#tabline#buffer_idx_mode = 1
let g:airline#extensions#tabline#buffer_nr_show = 1
nmap <leader>1 <Plug>AirlineSelectTab1
nmap <leader>2 <Plug>AirlineSelectTab2
nmap <leader>3 <Plug>AirlineSelectTab3
nmap <leader>4 <Plug>AirlineSelectTab4
nmap <leader>5 <Plug>AirlineSelectTab5
nmap <leader>6 <Plug>AirlineSelectTab6
nmap <leader>7 <Plug>AirlineSelectTab7
nmap <leader>8 <Plug>AirlineSelectTab8
nmap <leader>9 <Plug>AirlineSelectTab9

" NERD me from the beginning!
autocmd vimenter * NERDTree

" Silly SignColumn
highlight clear SignColumn

" AutoSave for the win!
let g:auto_save = 0
let g:auto_save_in_insert_mode = 0
let g:auto_save_no_updatetime = 1
